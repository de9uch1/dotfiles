set number
set cin
syntax on
filetype on
set backspace=start
set encoding=utf-8
set termencoding=utf-8
set ts=8
set sw=8

set autoindent
set expandtab
set softtabstop=4
set shiftwidth=4
set indentkeys=o,O
