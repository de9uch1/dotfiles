#!/bin/sh

if [ `which git` ] ; then
    if [ `ssh -T dlambda@bitbucket.org` ] ; then
        git clone git@bitbucket.org:dlambda/dotfiles.git ~/.dotfiles
    else
        git clone https://dlambda@bitbucket.org/dlambda/dotfiles.git ~/.dotfiles
    fi
elif [ `which wget` ] ; then
    mkdir /tmp/dotfiles
    cd /tmp/dotfiles
    wget https://bitbucket.org/dlambda/dotfiles/get/master.tar.gz
    tar xzf master.tar.gz
    mv * ~/.dotfiles
    rm -r /tmp/dotfiles
fi

cd ~/.dotfiles
if [ $(uname) = "Linux" ] ; then
    make link
elif [ $(uname) = "FreeBSD" ] ; then
    gmake link
fi
